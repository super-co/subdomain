require_relative 'lib/super'

require_relative 'gen/user_api_pb'

stub = UserApi::Stub.new('localhost:50051', :this_channel_is_insecure)

req = GetUserRequest.new(id: '1')
res = stub.get(req)
puts "res: #{res.to_json}"


stub = ReflectionApi.rpc_stub_class.new('localhost:50051', :this_channel_is_insecure)
res = stub.reflect(ReflectionRequest.new)
puts "res: #{res.to_json}"
