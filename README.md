# Subdomain

minimalistic api development based on GRPC, ActiveRecord to create micro api-s

#### define api-s

```api :name, :input_type, output_type```

example

```ruby
class UserApi < Super::Api

  api  :get, GetUserRequest, User
  def get(input)
    User.find(input.id)
  end

end
```

#### implement domain models 

from normal ruby object 

```ruby
class User < Super::Message
  field :id, String
  field :first_name, String
  field :last_name, String
end
```

from active record objects

```ruby
class User < ActiveRecord::Base
end
```

### usage
   
```ruby
client = UserApi::Stub.new('localhost:50051', :this_channel_is_insecure)

req = GetUserRequest.new(id: "")
client.get(req)
```

### 