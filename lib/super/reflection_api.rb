require_relative 'api'
require_relative 'reflection'

class ReflectionApi < Super::Api

  api :reflect, ReflectionRequest, ReflectionResponse
  def reflect(req, _)
    ReflectionResponse.new(definitions: ApiRegistry.instance.proto_definitions)
  end
end