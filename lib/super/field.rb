module Types
  class Float
    def self.to_proto
      'float'
    end
  end
  class Double
    def self.to_proto
      'double'
    end
  end
  class Int32
    def self.to_proto
      'int32'
    end
  end
  class Int64
    def self.to_proto
      'int64'
    end
  end
  class UInt32
    def self.to_proto
      'uint32'
    end
  end
  class UInt64
    def self.to_proto
      'uint64'
    end
  end
  class SInt32
    def self.to_proto
      'sint32'
    end
  end
  class SInt64
    def self.to_proto
      'sint64'
    end
  end
  class Fixed32
    def self.to_proto
      'fixed32'
    end
  end
  class Fixed64
    def self.to_proto
      'fixed32'
    end
  end
  class SFixed32
    def self.to_proto
      'sfixed32'
    end
  end
  class SFixed64
    def self.to_proto
      'sfixed32'
    end
  end
  class Bool
    def self.to_proto
      'bool'
    end
  end
  class String
    def self.to_proto
      'string'
    end
  end
  class Bytes
    def self.to_proto
      'bytes'
    end
  end
  class ID
    def self.to_proto
      'string'
    end
  end
end

class Field
  attr_accessor :name, :type, :tag

  def initialize(name, type, tag)
    @name = name
    @type = type
    @tag = tag
  end

  #TODO: implement tag
  def to_proto
    "#{@type.to_proto} #{@name} = #{@tag};"
  end

end

def convert_active_record_type_to_proto(active_record_type)
  case active_record_type.to_s
  when 'binary' then return 'bytes'
  when 'boolean'then return 'bool'
  when 'decimal'then return 'double'
  when 'float' then return 'float'
  when 'integer' then return 'int32'
  when 'bigint' then return 'int64'
  when 'string' then return 'string'
  when 'text' then return 'string'
  when 'date' then return 'int64'
  when 'datetime' then return 'int64'
  when 'time' then return 'int64'
  when 'timestamp' then return 'int64'
  else raise 'unsupported active record type'
  end
end
