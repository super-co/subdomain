class MethodDesc
  attr_reader :name, :input, :output

  def initialize(name, input, output)
    @name = name
    @input = input
    @output = output
  end

  def to_proto
    %{rpc #{@name} (#{@input}) returns (#{@output})}
  end
end


class ApiDesc
  attr_reader :name, :method_descs

  def initialize(name)
    @name = name
    @method_descs = []
  end

  def add_method(method)
    @method_descs << method
  end

  def to_proto
    message_definitions = @method_descs.map do |desc|
      "
        #{desc.input.to_proto}

        #{desc.output.to_proto}
      "
    end.join("\n")

    "
    #{message_definitions}

    service #{name} {
      #{@method_descs.map(&:to_proto).join('\n')
    }"
  end
end