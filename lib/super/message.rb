require 'active_support'

module Super

  class Message

    def initialize(values = {})
      @values = values
    end

    def decode(json)
      @values = ActiveSupport::JSON.decode(json)
    end

    def encode
      ActiveSupport::JSON.encode(@values)
    end

    class << self

      def fields
        @field ||= []
      end

      def field(name, type)
        next_tag = fields.size + 1
        fields << Field.new(name, type, next_tag)
      end

      def decode(json)
        new.decode(json)
      end

      def encode(message)
        message.encode
      end

      def to_proto
        %{message #{self.to_s} {
            #{fields.map(&:to_proto).join('\n')}
         }}
      end
    end

  end
end

