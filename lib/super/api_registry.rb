require 'singleton'

class ApiRegistry
  include Singleton

  def initialize
    @api_descs = []
  end

  def register(api_desc)
    @api_descs << api_desc
  end

  def add_method(api_name, method_desc)
    api_desc = @api_descs.find { |desc| desc.name == api_name }
    api_desc.add_method(method_desc)
  end

  def proto_definitions
    @api_descs.map(&:to_proto).join("\n")
  end

end