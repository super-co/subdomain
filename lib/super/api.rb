require 'grpc'

module Super
  class Api

    include GRPC::GenericService

    class << self
      def inherited(child)
        child.marshal_class_method = :encode
        child.unmarshal_class_method = :decode
        child.service_name = child.to_s

        ApiRegistry.instance.register(ApiDesc.new(child.service_name))
      end

      def api(method_name, input_type, return_type)
        rpc(method_name, input_type, return_type)

        ApiRegistry.instance.add_method(service_name, MethodDesc.new(method_name, input_type, return_type))
      end
    end

  end
end
