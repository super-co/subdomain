class ReflectionRequest < Super::Message

end

class ReflectionResponse < Super::Message
  field :definitions, Types::String
end