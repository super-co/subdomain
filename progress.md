# super api

super-api lets you create, compose and remix api-s so business could ship and scale products with ease.

We’ve been building api-s for businesses (from startups to enterprises) for a while, from doing that we observed that the current state of api development is a mess

- learning curve for building api-s is steep with tons of design patterns and best practices 
- lego blocks are missing, there is no way to fork, modify and compose pre-built api-s, so its reinventing the wheel many times 
- lost in infrastructure choices and details

As a result api-s require large dev teams or consulting firms to build and it might even turn out to be bad with high coupling, low extensibility and slow in performance. So we want to make this simple by giving businesses a api development toolkit comprising of

- a framework: curated with modern api technologies, best practices, automatic code generation, infrastructure builtins to write your api-s 
- ready made kits: prebuilt api-s authored by us, built using the framework, representing generic business domains that could be forked, modified and composed


### how does it work?

So to build a uber eats clone with super api you would

- create a namespace for all your api-s, we call it the superdomain 
- create api-s for specific business subdomains by forking and composing from our library of kits for identity, communication, order, invoice, payment, delivery and catalog management, we call these subdomains 
- expose all the subdomain using our gateway for web and mobile apps to consume## how does it work?

### progress

#### week 1

- Decide on the specs for the subdomain framework and gateway
- Bootstrap the subdomain framework
- Bootstrap the gateway

#### week 2

We're rapidly iterating on the subdomain framework 

- Implement an api type system with protobuf as transport
- Serialize normal ruby objects to protobuf
- Introduce active record for database support
- Serialize active record objects to protobuf
- Implement reflection to introspect the api-s

[see below for more details](https://gist.github.com/sunilvigneshr/ee973a1b02c5a279b01b0767d62cc42e#subdomain-framework)


### week 3

- Bootstrap the cli to scaffold project structure and generate clients and gateways
- Generate api clients, gateway schema and resolvers using the api reflection available in subdomain
- Connect subdomains and gateway
- Connect between subdomains
 

## subdomain framework

Minimalistic api development framework based on modern technologies and best practices which lets you create the fundamental building blocks of your api-s

#### define api-s

```api :name, :input_type, output_type```

example

```ruby
class UserApi < Super::Api

  api  :get, GetUserRequest, User
  def get(input)
    User.find(input.id)
  end

end
```

#### implement domain models 

from normal ruby object 

```ruby
class User < Super::Message
  field :id, String
  field :first_name, String
  field :last_name, String
end
```

from active record objects

```ruby
class User < ActiveRecord::Base
end
```

### usage

start the server

```ruby server.rb```

client side usage
   
```ruby
client = UserApi::Stub.new('localhost:50051', :this_channel_is_insecure)

req = GetUserRequest.new(id: "")
client.get(req)
```

##### gateway

TBD

##### kits

TBD

#### cli

TBD

#### superdomain

