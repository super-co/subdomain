Gem::Specification.new do |s|
  s.name = 'super'
  s.version = '0.1.0'
  s.summary = 'super framework'
  s.authors = ['super co']
  s.files = Dir['lib/**/*']
  s.add_runtime_dependency 'grpc', '1.19.0'
  s.add_runtime_dependency 'protobuf', '3.10.0'
  s.add_runtime_dependency 'activesupport', '5.2.3'
end
