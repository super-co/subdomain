class User < ActiveRecord::Base

  class << self

    def encode(obj)
      obj.to_json
    end


    def decode(json)
      User.new(ActiveSupport::JSON.decode(json))
    end

    def to_proto
      %{
        message User {
          #{User.columns_hash.values.map.with_index(1) { |column, index| "#{convert_active_record_type_to_proto(column.type)} #{column.name} = #{index};" }.join('\n')}
        }
      }
    end

  end
end
