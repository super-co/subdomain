#!/usr/bin/env ruby


# TODO: autoload these libraries
require 'grpc'

require_relative 'lib/super'


require 'active_record'

ActiveRecord::Base.establish_connection(
    adapter: "sqlite3",
    dbfile: ":memory:",
    database: "users.sqlite"
)

# ActiveRecord::Schema.define do
#   create_table :users do |table|
#     table.column :first_name, :string
#     table.column :last_name, :string
#   end
# end

require_relative 'domain/get_user_request'
require_relative 'domain/user'

require_relative 'api/user_api'

p User.to_proto

addr = '0.0.0.0:50051'
server = GRPC::RpcServer.new
server.add_http2_port(addr, :this_port_is_insecure)
server.handle(UserApi)
server.handle(ReflectionApi)
server.run_till_terminated_or_interrupted([1, 'int', 'SIGQUIT'])

